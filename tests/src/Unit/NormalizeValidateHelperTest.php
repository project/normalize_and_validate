<?php

namespace Drupal\Tests\normalize_and_validate\Unit;

use Drupal\Component\Utility\EmailValidator;
use Drupal\normalize_and_validate\NormalizeValidateHelper;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the "openstates_address_to_array" process plugin.
 *
 * @group newmode
 * @coversDefaultClass \Drupal\normalize_and_validate\NormalizeValidateHelper
 */
class NormalizeValidateHelperTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $emailValidator = new EmailValidator();
    $service = new NormalizeValidateHelper($emailValidator);
    $container = new ContainerBuilder();
    \Drupal::setContainer($container);
    $container->set('normalize_and_validate.normalize_validate_helper', $service);
  }

  /**
   * Checks if the service is created in the Drupal context.
   */
  public function testService() {
    $this->assertNotNull(\Drupal::service('normalize_and_validate.normalize_validate_helper'));
  }

  /**
   * Data provider for testnormalizeValidate().
   *
   * @return array
   *   An array containing input values and expected output values.
   */
  public function normalizeValidateDataProvider(): array {
    return [
      'boolean_str_1' => [
        'input_type' => 'boolean',
        'input' => '1',
        'context' => 'international',
        // Return value will be $response->output = TRUE (boolean), and after
        // transform to string, it will be string 'true'.
        'expected_output' => 'true',
      ],
      'boolean_str_true' => [
        'input_type' => 'boolean',
        'input' => 'True',
        'context' => 'international',
        // Return value will be $response->output = TRUE (boolean), and after
        // transform to string, it will be string 'true'.
        'expected_output' => 'true',
      ],
      'boolean_str_0' => [
        'input_type' => 'boolean',
        'input' => '0',
        'context' => 'international',
        // Return value will be $response->output = FALSE (boolean), and after
        // transform to string, it will be string 'false'.
        'expected_output' => 'false',
      ],
      'phone_space_and_dash' => [
        'input_type' => 'phone',
        'input' => '   01-514-666-6666',
        'context' => 'international',
        'expected_output' => '15146666666',
      ],
      'phone_too_short' => [
        'input_type' => 'phone',
        'input' => '014',
        'context' => 'international',
        'expected_output' => '',
      ],
      'email_invalid' => [
        'input_type' => 'email',
        'input' => 'jane@example sfdgggggg4545356',
        'context' => 'international',
        'expected_output' => '',
      ],
      'email_space' => [
        'input_type' => 'email',
        'input' => '    jim@example.com',
        'context' => 'international',
        'expected_output' => 'jim@example.com',
      ],
      'twitter_space' => [
        'input_type' => 'twitter',
        'input' => '@example ',
        'context' => 'international',
        'expected_output' => 'example',
      ],
      'twitter_url' => [
        'input_type' => 'twitter',
        'input' => 'https://twitter.com/NewModeInc',
        'context' => 'international',
        'expected_output' => 'NewModeInc',
      ],
      'city_abbr_space' => [
        'input_type' => 'city',
        'input' => 'Pt. St. Charles ',
        'context' => 'international',
        'expected_output' => 'Point Saint Charles',
      ],
      'city_abbr_short' => [
        'input_type' => 'city',
        'input' => 'LA',
        'context' => 'international',
        'expected_output' => 'Los Angeles',
      ],
      'postalcode_canada' => [
        'input_type' => 'postalcode',
        'input' => 'V0V 1A0',
        'context' => 'CA',
        'expected_output' => 'V0V1A0',
      ],
      'postalcode_intl_no_validation' => [
        'input_type' => 'postalcode',
        'input' => 'ABC 90210',
        'context' => 'international',
        'expected_output' => 'ABC90210',
      ],
      // Note that postal code like 00000 is invalid (lowest should be 00501)
      // but our validator cannot tell that. The validation is just matching by
      // the number / char patterns.
      'postalcode_us_valid' => [
        'input_type' => 'postalcode',
        'input' => '10001',
        'context' => 'US',
        'expected_output' => '10001',
      ],
      'postalcode_us_valid_plus_4' => [
        'input_type' => 'postalcode',
        'input' => '10001-1234',
        'context' => 'US',
        'expected_output' => '10001-1234',
      ],
      'postalcode_us_invalid' => [
        'input_type' => 'postalcode',
        'input' => 'ABC 90210',
        'context' => 'US',
        'expected_output' => '',
      ],
      'legislative_position_senator_with_class' => [
        'input_type' => 'legislative_position',
        'input' => 'Senator, 1st Class',
        'context' => 'international',
        'expected_output' => 'Senator',
      ],
      'legislative_position_senator_with_country' => [
        'input_type' => 'legislative_position',
        'input' => 'U.S. Senator',
        'context' => 'international',
        'expected_output' => 'Senator',
      ],
      'legislative_position_senator_lower_case' => [
        'input_type' => 'legislative_position',
        'input' => 'senator',
        'context' => 'international',
        'expected_output' => 'Senator',
      ],
      'jurisdiction_us_state_abbr_2_letters' => [
        'input_type' => 'jurisdiction',
        'input' => 'WA',
        'context' => 'international',
        'expected_output' => 'WA',
      ],
      'jurisdiction_au_state_abbr_3_letters' => [
        'input_type' => 'jurisdiction',
        'input' => 'NSW',
        'context' => 'international',
        'expected_output' => 'NSW',
      ],
      'jurisdiction_au_long_uppercase' => [
        'input_type' => 'jurisdiction',
        'input' => 'SOUTH COAST',
        'context' => 'international',
        'expected_output' => 'South Coast',
      ],
      'jurisdiction_ca_qc_special_chars' => [
        'input_type' => 'jurisdiction',
        'input' => 'Rivière-du-Loup–Témiscouata',
        'context' => 'international',
        'expected_output' => 'Rivière-du-Loup–Témiscouata',
      ],
      'jurisdiction_city_ward_number' => [
        'input_type' => 'jurisdiction',
        'input' => '11',
        'context' => 'international',
        'expected_output' => 'District 11',
      ],
      'jurisdiction_au_municpal' => [
        'input_type' => 'jurisdiction',
        'input' => 'Adelaide (C)',
        'context' => 'international',
        'expected_output' => 'Adelaide',
      ],
      'unknown_input_type' => [
        'input_type' => 'something',
        'input' => 'something else',
        'context' => 'international',
        'expected_output' => '',
      ],
    ];
  }

  /**
   * Checks if the normalizeValidate is working as expected.
   *
   * @param string $input_type
   *   The input type.
   * @param mixed $input
   *   The input values.
   * @param string $context
   *   The context (mostly for postal code).
   * @param mixed $expected_output
   *   The expected output.
   *
   * @dataProvider normalizeValidateDataProvider
   */
  public function testnormalizeValidate(string $input_type, $input, string $context, $expected_output) {
    $service = \Drupal::service('normalize_and_validate.normalize_validate_helper');
    // The service could use t() function, needs sStringTranslationStub to make
    // it work in unit test context.
    $service->setStringTranslation($this->getStringTranslationStub());
    $output = (string) ($service->normalizeValidate($input_type, $input, $context));
    $this->assertSame($output, $expected_output);
  }

}
