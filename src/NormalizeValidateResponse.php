<?php

namespace Drupal\normalize_and_validate;

/**
 * Provides normalization and validation response.
 */
class NormalizeValidateResponse {

  /**
   * Validation and Normalization type.
   *
   * @var string
   */
  private $type;

  /**
   * Validation and Normalization input.
   *
   * @var string|bool|int
   */
  private $input;

  /**
   * Validation and Normalization context.
   *
   * @var string
   */
  private $context;

  /**
   * Validation and Normalization output.
   *
   * @var string|bool|int
   */
  private $output;

  /**
   * Is output valid?
   *
   * @var bool
   */
  private $valid;

  /**
   * Has output been normalized normalized?
   *
   * @var bool
   */
  private $normalized;

  /**
   * Validation response.
   *
   * @var string
   */
  private $validationResponse;

  /**
   * Normalization response.
   *
   * @var string
   */
  private $normalizationResponse;

  /**
   * NormalizeValidateResponse constructor.
   */
  final public function __construct($type, $input, $context = 'international') {
    $this->type = $type;
    $this->input = $input;
    $this->context = $context;
    $this->normalized = FALSE;
    $this->valid = TRUE;
  }

  /**
   * Type getter,.
   */
  public function type() {
    return $this->type;
  }

  /**
   * Input getter.
   */
  public function input() {
    return $this->input;
  }

  /**
   * Context getter.
   */
  public function context() {
    return $this->context;
  }

  /**
   * Output getter.
   */
  public function output() {
    return $this->output;
  }

  /**
   * Output setter.
   */
  public function setOutput($output) {
    $this->output = $output;
  }

  /**
   * Valid getter.
   */
  public function isValid() {
    return $this->valid;
  }

  /**
   * Valid setter.
   */
  public function setValid($valid) {
    $this->valid = $valid;
  }

  /**
   * Normalized getter.
   */
  public function isNormalized() {
    return $this->normalized;
  }

  /**
   * Normalized setter,.
   */
  public function setNormalized($normalized) {
    $this->normalized = $normalized;
  }

  /**
   * Validation responsse getter.
   */
  public function validationResponse() {
    return $this->validationResponse;
  }

  /**
   * Validation response setter.
   */
  public function setValidationResponse($validationResponse) {
    $this->validationResponse = $validationResponse;
  }

  /**
   * Normalization response getter.
   */
  public function normalizationResponse() {
    return $this->normalizationResponse;
  }

  /**
   * Normalization response setter.
   */
  public function setNormalizationResponse($normalizationResponse) {
    $this->normalizationResponse = $normalizationResponse;
  }

  /**
   * Magic method to convert to sting.
   */
  public function __toString() : string {
    if ($this->output === TRUE || $this->output === FALSE) {
      return json_encode($this->output);
    }
    elseif (empty($this->output)) {
      return '';
    }
    elseif (is_string($this->output)) {
      return $this->output;
    }
    else {
      return json_encode($this->output);
    }
  }

  /**
   * Normalization response setter.
   */
  public function toHtml() {
    $data = [
      'Type' => $this->type(),
      'Input' => $this->input(),
      'Context' => $this->context(),
      'Input is valid?' => $this->isValid() ? t('yes') : t('no'),
      'Validation details' => $this->validationResponse(),
      'Output' => $this->output(),
      'Output is normalized?' => $this->isNormalized() ? t('yes') : t('no'),
      'Normalization details' => $this->normalizationResponse(),

    ];

    $string = '<p class="nv-response"><ul>';
    foreach ($data as $title => $value) {
      $string .= '<li><strong>' . $title . '</strong>: <pre style="display:inline">[' . $value . ']</pre></li>';
    }
    $string .= "</ul></p>";
    return $string;
  }

}
