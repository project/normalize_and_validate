<?php

namespace Drupal\normalize_and_validate;

use CommerceGuys\Addressing\AddressFormat\AddressFormatRepository;
use CommerceGuys\Addressing\Validator\Constraints\AddressFormatConstraint;
use Drupal\Component\Utility\EmailValidator;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides normalization and validation for data..
 */
class NormalizeValidateHelper {

  use StringTranslationTrait;

  /**
   * The email validator.
   *
   * @var \Egulias\EmailValidator\EmailValidator
   */
  protected $emailValidator;

  /**
   * Construct.
   *
   * @param \Drupal\Component\Utility\EmailValidator $email_validator
   *   The email validator.
   */
  public function __construct(EmailValidator $email_validator) {
    $this->emailValidator = $email_validator;
  }

  /**
   * Normalize and validate for given input and type.
   */
  public function normalizeValidate($type, $input, $context = 'international') {
    $response = new NormalizeValidateResponse($type, $input, $context);

    switch ($type) {
      case 'boolean':
        $response = $this->normalizeValidateBoolean($response);
        break;

      case 'phone':
        $response = $this->normalizeValidatePhone($response);
        break;

      case 'email':
        $response = $this->normalizeValidateEmail($response);
        break;

      case 'twitter':
        $response = $this->normalizeValidateTwitter($response);
        break;

      case 'city':
        $response = $this->normalizeValidateCity($response);
        break;

      case 'postalcode':
        $response = $this->normalizeValidatePostalCode($response);
        break;

      case 'legislative_position':
        $response = $this->normalizeValidateLegislativePosition($response);
        break;

      case 'jurisdiction':
        $response = $this->normalizeValidateJurisdiction($response);
        break;

      default:
        $response->setValid(FALSE);
        $response->setValidationResponse($this->t('Invalid normalization and validation type.'));
        break;

    };

    return $response;
  }

  /**
   * Normalize and validate a given opt out value.
   */
  private function normalizeValidateBoolean($response) {
    if ($response->input() === TRUE
      || $response->input() === FALSE) {
      $response->setOutput($response->input());
    }
    else {
      switch (strtolower($response->input())) {
        case 'true':
        case '1':
        case 1:
        case 'yes':
          $response->setNormalized(TRUE);
          $response->setOutput(TRUE);
          break;

        case 'false':
        case '0':
        case 0:
        case 'no':
          $response->setNormalized(TRUE);
          $response->setOutput(FALSE);
          break;

        default:
          $response->setValid(FALSE);
          $response->setValidationResponse($this->t('Opt out can only be true/false, 1/0 or yes/no.'));
          $response->setOutput($response->input);
      }
    }
    return $response;
  }

  /**
   * Normalize and validate a given phone number.
   */
  private function normalizeValidatePhone($response) {
    $orig_phone_number = $response->input();
    // Remove any non-numeric characters.
    $phone_number = preg_replace("/[^0-9]/", "", $orig_phone_number);

    // @todo add extension support once we support them in call tools.
    // Process non-empty numbers.
    if (!empty($orig_phone_number)) {
      // Remove leading zeros.
      while (isset($phone_number[0]) && $phone_number[0] == '0') {
        $phone_number = substr($phone_number, 1);
      }

      // @todo context-specific validation and context parsing from
      // numeric code.  If less than 4 characters then set value to null.
      if (strlen($phone_number) < 3) {
        $response->setValid(FALSE);
        $return['issue'] = $this->t('Invalid phone number removed: @number', ['@number' => $phone_number]);
        $phone_number = '';
      }
    }

    // Mark as normalized if the value has changed.
    if ($phone_number !== $response->input()) {
      $response->setNormalized(TRUE);
    }

    // Pass back processed phone value.
    $response->setOutput($phone_number);

    return $response;
  }

  /**
   * Normalize and validate a given email address.
   */
  private function normalizeValidateEmail($response) {
    $email_address = $response->input();

    // Trim.
    $email_address = trim($email_address);

    // Lower case.
    $email_address = strtolower($email_address);

    // Validate email.
    // @phpstan-ignore-next-line
    if (!$this->emailValidator->isValid($email_address)) {
      $email_address = '';
      $response->setValid(FALSE);
      $response->setNormalizationResponse($this->t('Invalid email address removed: @email', ['@email' => $email_address]));
      $response->setValidationResponse($this->t('Invalid email address: @email', ['@email' => $email_address]));
    }

    // Mark as normalized if the value has changed.
    if ($email_address !== $response->input()) {
      $response->setNormalized(TRUE);
    }

    // Pass back processed email value.
    $response->setOutput($email_address);

    return $response;
  }

  /**
   * Normalize and validate a given twitter handle.
   */
  private function normalizeValidateTwitter($response) {
    $handle = $response->input();

    // Remove URL bits and pieces if handle is URL.
    $exploder = explode('/', $handle);
    if (count($exploder) > 1) {
      $handle = trim(end($exploder));
    }

    // Remove @ symbol.
    $handle = str_replace('@', '', $handle);

    // Remove spaces.
    $handle = str_replace(' ', '', $handle);

    // Ignore underscores.
    $valid = ['_'];

    // Check for alphanumeric.
    if (!ctype_alnum(str_replace($valid, '', $handle)) && !empty($handle)) {
      $response->setValid(FALSE);
      $response->setNormalizationResponse($this->t('Invalid twitter handle removed: @handle', ['@handle' => $handle]));
      $response->setValidationResponse($this->t('Twitter handle is not alphanumeric.'));
      $handle = '';
    }
    // Check for over max length.
    elseif (strlen($handle) > 15 && !empty($handle)) {
      $response->setValid(FALSE);
      $response->setNormalizationResponse($return['issue'] = $this->t('Invalid twitter handle removed: @handle', ['@handle' => $handle]));
      $response->setValidationResponse($this->t('Twitter handle is greater than the maximum length of 15 characters.'));
      $handle = '';
    }

    // Mark as normalized if the value has changed.
    if ($handle !== $response->input()) {
      $response->setNormalized(TRUE);
    }

    // Pass back processed handle.
    $response->setOutput($handle);

    return $response;
  }

  /**
   * Normalize and validate a given city name.
   */
  private function normalizeValidateCity($response) {
    $city = $response->input();

    // Remove accent chars, if there's any, so that
    // it's easier to compare.
    $city = iconv('UTF-8', 'ASCII//TRANSLIT', $city);

    // Move to upper case for processing.
    $city = strtoupper($city);

    // Replace abbreviations and the like.
    $replacements = [
      'SAINT' => 'ST',
      'POINT' => 'PT',
      'LOS ANGELES' => 'LA',
      'NEW YORK' => 'NYC',
    ];
    foreach ($replacements as $replace => $search) {
      $city = str_replace($search, $replace, $city);
    }

    // Repalce dashes and the like.
    $stop_words = [
      '.',
      '-',
      '_',
    ];
    foreach ($stop_words as $search) {
      $city = str_replace($search, '', $city);
    }

    // Replace multiple spaces with single.
    $city = preg_replace('!\s+!', ' ', $city);

    // Trim and make it look nicer.
    $city = trim($city);
    $city = strtolower($city);
    $city = ucwords($city);

    // Mark as normalized if the value has changed.
    if ($city !== $response->input()) {
      $response->setNormalized(TRUE);
    }

    // Pass back processed city name.
    $response->setOutput($city);

    return $response;
  }

  /**
   * Normalize and validate a given postal code.
   */
  private function normalizeValidatePostalCode($response) {
    $postalCode = $response->input();

    // Remove any internal spaces.
    $postalCode = trim(str_replace(" ", "", $postalCode));

    // Upper case any letters.
    $postalCode = strtoupper($postalCode);

    if ($response->context() !== 'international') {
      $countryCode = $response->context();

      $valid = TRUE;
      $normalizationResponseText = '';
      $validationResponseText = '';
      // Mostly copied and modifed from
      // AddressFormatConstraintValidator::validatePostalCode()
      // which is a protected function.
      $constraint = new AddressFormatConstraint();
      $addressFormatRepository = new AddressFormatRepository();
      $addressFormat = $addressFormatRepository->get($countryCode);

      // Also, we should mark empty postal code as invalid, only validate on
      // country level (no subdivision level or tied to street address).
      if (empty($postalCode)) {
        $valid = FALSE;
        $validationResponseText = $this->t('Empty postal code is invalid.');
      }
      else {
        // Resolve the available patterns.
        $fullPattern = $addressFormat->getPostalCodePattern();
        if ($fullPattern) {
          // The pattern must match the provided value completely.
          preg_match('/' . $fullPattern . '/i', $postalCode, $matches);
          if (!isset($matches[0]) || $matches[0] !== $postalCode) {
            $valid = FALSE;
            $normalizationResponseText = $this->t('Invalid postal code removed: @postal_code', ['@postal_code' => $postalCode]);
            $validationResponseText = $this->t('Invalid postal code.');
          }
        }
        // The startPattern / subdivision match is removed as we don't use it.
        // It can be used to match something like "Vancouver" starts with "V".
      }

      // Flag validation issues.
      if (!$valid) {
        $response->setValid(FALSE);
        $response->setNormalizationResponse($normalizationResponseText);
        $response->setValidationResponse($validationResponseText);
        $postalCode = '';
      }
    }

    // Mark as normalized if the value has changed.
    if ($postalCode !== $response->input()) {
      $response->setNormalized(TRUE);
    }

    // Pass back processed handle.
    $response->setOutput($postalCode);

    return $response;
  }

  /**
   * Normalize and validate a legislative position (Rep, Senate etc.).
   */
  private function normalizeValidateLegislativePosition($response) {
    $position = $response->input();

    // Remove accent chars, if there's any, so that
    // it's easier to compare.
    $position = iconv('UTF-8', 'ASCII//TRANSLIT', $position);

    // Move to upper case for processing.
    $position = strtoupper($position);

    // Replace abbreviations and the like.
    $replacements = [
      'U.S. SENATOR' => 'SENATOR',
      'STATE SENATOR' => 'SENATOR',
      'SENATOR, 1ST CLASS' => 'SENATOR',
      'SENATOR, 2ND CLASS' => 'SENATOR',
      'SENATOR, 3RD CLASS' => 'SENATOR',
      'STATE ASSEMBLYMEMBER' => 'STATE REPRESENTITIVE',
      'ALDERPERSONPERSON' => 'ALDERPERSON',
    ];
    foreach ($replacements as $search => $replace) {
      $position = str_replace($search, $replace, $position);
    }

    // Trim and make it look nicer.
    $position = trim($position);
    $position = strtolower($position);
    $position = ucwords($position);

    // Mark as normalized if the value has changed.
    if ($position !== $response->input()) {
      $response->setNormalized(TRUE);
    }

    // Pass back processed city name.
    $response->setOutput($position);

    return $response;
  }

  /**
   * Normalize and validate a jurisdiction name.
   */
  private function normalizeValidateJurisdiction($response) {
    $jurisdiction = $response->input();

    // Remove Australian municipal type designations.
    $jurisdiction = str_replace(" (A)", "", $jurisdiction);
    $jurisdiction = str_replace(" (C)", "", $jurisdiction);
    $jurisdiction = str_replace(" (M)", "", $jurisdiction);
    $jurisdiction = str_replace(" (R)", "", $jurisdiction);
    $jurisdiction = str_replace(" (S)", "", $jurisdiction);
    $jurisdiction = str_replace(" (T)", "", $jurisdiction);
    $jurisdiction = str_replace(" (DC)", "", $jurisdiction);
    $jurisdiction = str_replace(" (RC)", "", $jurisdiction);

    // Trim and make it look nicer.
    $jurisdiction = trim($jurisdiction);

    // Make "1" display as "District 1".
    if (is_numeric($jurisdiction)) {
      $jurisdiction = "District " . $jurisdiction;
    }
    // If it's longer than 3 chars and all uppercase, capitalize first word
    // and lower the rest.
    // Shorter words with all uppercase are likely abbreviations such as:
    // - BC (Canada).
    // - WA (US).
    // - NSW (Australia).
    // And should not be lowercased.
    elseif ((strlen($jurisdiction) > 3)) {
      // The function ctype_upper() treats space, dash, underscrore etc as not
      // uppercase, even all other chars are uppercase, so we use strtoupper()
      // to check instead.
      if (strtoupper($jurisdiction) == $jurisdiction) {
        $jurisdiction = strtolower($jurisdiction);
        $jurisdiction = ucwords($jurisdiction);
      }
    }

    // Mark as normalized if the value has changed.
    if ($jurisdiction !== $response->input()) {
      $response->setNormalized(TRUE);
    }

    // Pass back processed jurisdiction name.
    $response->setOutput($jurisdiction);
    return $response;
  }

}
