<?php

namespace Drupal\normalize_and_validate\Controller;

/**
 * Provides example of normalization and validation service usage.
 */
class NormalizeValidateExample {

  /**
   * Examples of some normalization and validation calls in action.
   */
  public function example() {
    $output = '';

    $normalizeValidateHelper = \Drupal::service('normalize_and_validate.normalize_validate_helper');

    // Boolean example.
    $response = $normalizeValidateHelper->normalizeValidate('boolean', 'True');
    $output .= $response->toHtml();
    $output .= '<hr />';

    // Phone examples.
    $response = $normalizeValidateHelper->normalizeValidate('phone', '   01-514-666-6666');
    $output .= $response->toHtml();
    $response = $normalizeValidateHelper->normalizeValidate('phone', '014');
    $output .= $response->toHtml();
    $output .= '<hr />';

    // Email examples.
    $response = $normalizeValidateHelper->normalizeValidate('email', 'jane@example sfdgggggg4545356');
    $output .= $response->toHtml();
    $response = $normalizeValidateHelper->normalizeValidate('email', '    jim@example.com');
    $output .= $response->toHtml();
    $output .= '<hr />';

    // Twitter examples.
    $response = $normalizeValidateHelper->normalizeValidate('twitter', '@example ');
    $output .= $response->toHtml();
    $response = $normalizeValidateHelper->normalizeValidate('twitter', 'https://twitter.com/NewModeInc');
    $output .= $response->toHtml();
    $output .= '<hr />';

    // City examples.
    $response = $normalizeValidateHelper->normalizeValidate('city', 'Pt. St. Charles ');
    $output .= $response->toHtml();
    $response = $normalizeValidateHelper->normalizeValidate('city', 'LA');
    $output .= $response->toHtml();
    $output .= '<hr />';

    // Postalcode examples. Notes that this still needs some work.
    $response = $normalizeValidateHelper->normalizeValidate('postalcode', 'V0V 1A0 ', 'CA');
    $output .= $response->toHtml();
    $response = $normalizeValidateHelper->normalizeValidate('postalcode', '90210');
    $output .= $response->toHtml();
    $response = $normalizeValidateHelper->normalizeValidate('postalcode', '10001', 'US');
    $output .= $response->toHtml();
    $output .= '<hr />';

    // Legislative position examples.
    $output .= "<h3>Legislative position examples</h3>";
    $response = $normalizeValidateHelper->normalizeValidate('legislative_position', 'Senator, 1st Class');
    $output .= $response->toHtml();
    $output .= '<hr />';
    $response = $normalizeValidateHelper->normalizeValidate('legislative_position', 'Senator, 2nd Class');
    $output .= $response->toHtml();
    $output .= '<hr />';
    $response = $normalizeValidateHelper->normalizeValidate('legislative_position', 'Senator, 3rd Class');
    $output .= $response->toHtml();
    $output .= '<hr />';
    $response = $normalizeValidateHelper->normalizeValidate('legislative_position', 'U.S. Senator');
    $output .= $response->toHtml();
    $output .= '<hr />';
    $response = $normalizeValidateHelper->normalizeValidate('legislative_position', 'senator');
    $output .= $response->toHtml();
    $output .= '<hr />';

    return [
      '#markup' => $output,
    ];
  }

}
