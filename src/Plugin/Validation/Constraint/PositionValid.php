<?php

namespace Drupal\normalize_and_validate\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a valid position.
 *
 * @Constraint(
 *   id = "PositionValid",
 *   label = @Translation("Valid position", context = "Validation"),
 *   type = "string"
 * )
 */
class PositionValid extends Constraint {

  /**
   * The message that will be shown if the value is not a valid position.
   *
   * @var string
   */
  public $notValid = '%value is not a valid position';

}
