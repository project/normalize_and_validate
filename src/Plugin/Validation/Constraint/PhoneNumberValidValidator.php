<?php

namespace Drupal\normalize_and_validate\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\normalize_and_validate\NormalizeValidateHelper;

/**
 * Validates the PhoneNumberValid constraint.
 */
class PhoneNumberValidValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The normalizer.
   *
   * @var \Drupal\normalize_and_validate\NormalizeValidateHelper
   */
  protected $normalizer;

  /**
   * Constructor.
   *
   * @param \Drupal\normalize_and_validate\NormalizeValidateHelper $normalizer
   *   The normalizer.
   */
  final public function __construct(NormalizeValidateHelper $normalizer) {
    $this->normalizer = $normalizer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('normalize_and_validate.normalize_validate_helper'));
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    foreach ($items as $item) {
      $response = $this->normalizer->normalizeValidate('phone', $item->value);
      if (!$response->isValid() && isset($constraint->notValid)) {
        $this->context->addViolation($constraint->notValid, ['%value' => $item->value]);
      }

    }
  }

}
