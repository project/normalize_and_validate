<?php

namespace Drupal\normalize_and_validate\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a valid phone number.
 *
 * @Constraint(
 *   id = "PhoneNumberValid",
 *   label = @Translation("Valid phone number", context = "Validation"),
 *   type = "string"
 * )
 */
class PhoneNumberValid extends Constraint {

  /**
   * The message that will be shown if the value is not a valid phone number.
   *
   * @var string
   */
  public $notValid = '%value is not a valid phone number';

}
